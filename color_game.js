//Arrays: colores y divs
var color = []
var cuadraditos = document.querySelectorAll('.square')
//El querySelectorAll forma un array que se puede recorrer
var largoArray = 6

//Definición de variables varias
var mensaje = document.querySelector('#message')
var titulo = document.querySelector('.title')
var colorDisplay = document.querySelector('#colorDisplay')
var botonReset = document.querySelector('#reset')
var clickedColor
var pickedColor

//Definición del color random: array y color elegido
var randomColor = function () {
    var color1 = Math.round(Math.random() * 225)
    var color2 = Math.round(Math.random() * 225)
    var color3 = Math.round(Math.random() * 225)
    var colorRandom = 'rgb(' + color1 + ', ' + color2 + ', ' + color3 + ')'
    return colorRandom
}
randomColor()

//Generación de array color+color elegido
var generateRandomColors = function (largoArray) {
    for (var index = 0; index < largoArray; index++) {
        color.push(randomColor())
    }
}
generateRandomColors(largoArray)
pickedColor = color[Math.floor(Math.random() * largoArray)]
colorDisplay.textContent = pickedColor



//Función para asignar color a los div
var cuadrados = function() {
  for (var i = 0; i < cuadraditos.length; i++) {
        if(!color[i]) {
        cuadraditos[i].style.display = 'none';
        }
        else {
        cuadraditos[i].style.background = color[i];
        cuadraditos[i].style.display = 'block';
        }
}
}
cuadrados()

//Función para agregar evento a cada div
var clickear = function() {
  for (var i = 0; i < cuadraditos.length; i++) {
  cuadraditos[i].addEventListener("click", function (event) {
  clickedColor = event.target.style.background
  if (clickedColor === pickedColor) {
        mensaje.textContent = 'Correct!'
        titulo.style.background = clickedColor
        botonReset.textContent= 'Play again?'

        for (var i = 0; i < cuadraditos.length; i++) {
              if(!color[i]) {
              cuadraditos[i].style.display = 'none';
              }
              else {
              cuadraditos[i].style.background = pickedColor;
              cuadraditos[i].style.display = 'block';
              }
      }

      }
  else {this.style.background = '#232323'
        mensaje.textContent = 'Try Again!'
      }

    })
  }
}
clickear()

//Evento Botón
botonReset.addEventListener('click', function (click) {
    for (var index = 0; index < largoArray; index++) {
        color.pop()
    }
    titulo.style.background = 'steelblue'
    mensaje.textContent= ' '
    botonReset.textContent='New Colors'
    generateRandomColors(largoArray)
    pickedColor = color[Math.round(Math.random() * largoArray)]
    colorDisplay.textContent = pickedColor
    cuadrados()
})


//Evento botones: easy/hard
var botonEasy = document.querySelector('#easy')
var botonHard = document.querySelector('#hard')


botonHard.addEventListener('click', function() {
  botonEasy.classList.remove('selected');
  botonHard.classList.add('selected');
  for (var index = 0; index < largoArray; index++) {
      color.pop()
  }
  largoArray=6;
  titulo.style.background = 'steelblue';
  mensaje.textContent= ' ';
  generateRandomColors(largoArray);
  pickedColor = color[Math.round(Math.random() * largoArray)];
  colorDisplay.textContent = pickedColor;
  cuadrados()
})

botonEasy.addEventListener('click', function() {
  botonEasy.classList.add('selected')
  botonHard.classList.remove('selected')
  for (var index = 0; index < largoArray; index++) {
      color.pop()
  }
  largoArray=3;
  titulo.style.background = 'steelblue';
  mensaje.textContent= ' ';
  generateRandomColors(largoArray);
  pickedColor = color[Math.round(Math.random() * largoArray)];
  colorDisplay.textContent = pickedColor;
  cuadrados();
})
